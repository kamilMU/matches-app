import React from 'react'

class SidebarTab extends React.Component {

    render() {
        return( 
            <>
                <div className="sidebar-country" onClick={this.props.onClick}>{this.props.tabName}</div>
                {this.props.showContent && <div>{this.props.content.map(cupName => 
                    <div className="cupname-and-delete" key={cupName.id}>
                        <div>{cupName.item}</div>
                        <div onClick={() => {this.props.deleteCupName(cupName.id)}} style={{cursor: 'pointer'}}>x</div>
                    </div>)}
                </div>}
            </>
        )
    }
}

export default SidebarTab;

import React from 'react';
import SidebarTab from './SidebarTab';

class MyLeagueTabs extends React.Component {
  state = {
    leagues: this.props.leagues,
    countries: this.props.leagues,
    activeTab: null,
    activeCountryTab: null
  }

  deleteCupName = async (championshipId) => {
    this.setState({
      leagues: this.state.leagues.map(league => { 
        return {
          ...league,
          items: league.items.filter(championship => championship.id !== championshipId)
        }
      })
    })
    
  }

  componentWillReceiveProps(nexProps) {
    if(nexProps.leagues.length !== this.props.leagues.length){
      this.setState({
        leagues: nexProps.leagues
      })
    }
  }
  openTab = tabName => {
    return () => {
      if(this.state.activeTab !== tabName) {
        this.setState({
          activeTab: tabName
        })
      }else{
        this.setState({
          activeTab: null
        })
      }
    }
  }

  render() {
    return (
        <div className="side-bar-block">
          <div className="side-bar-my-leagues">
            <h2 className="side-bar-tittle">Мои Лиги</h2>
              <div className="sidebar-leagues-list">
                {this.state.leagues.map(league =>
                <div key={league.id}>
                  <SidebarTab 
                    key={league.id}
                    tabName={league.league} 
                    content={league.items} 
                    deleteCupName={this.deleteCupName} 
                    onClick={this.openTab(league.league)}
                    showContent={this.state.activeTab === league.league} 
                  />
                </div>  
                )}
              </div>
          </div>
        </div>
        )
  }
}

export default MyLeagueTabs;

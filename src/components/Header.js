import React from 'react';
import { NavLink } from 'react-router-dom'
import menu from '../flag/menu.jpg'
import searchImg from '../flag/Vector.jpg'

const Header = ({ toggle, isOpen }) => {
  return (
    <>
        <header className="header">
            <div className="header-wrap">
                <div className="header-logo">
                    <h2 className="header-tittle">Расписание матчей</h2>
                </div>
                <div className="header-main-tabs">
                    <div className="header-menu">
                        <img src={menu} alt="menu" width="40" height="40" className="menu" onClick={toggle} />
                    </div>
                    <div className={isOpen ? "openned-header-tabs" : "header-tabs"}>
                        <div className="mobile-menu">
                            <div className="menu__tittle">Меню</div>
                            <div className="menu__close" onClick={toggle}>x</div>
                        </div>
                        <div className="header-tabs__all"><NavLink to='/all' activeClassName='active' className='header-tab'>Все</NavLink></div>
                        <div className="header-tabs__live">Live</div>
                        <div className="header-tabs__results">Результаты</div>
                        <div className="header-tabs__shedule">Расписание</div>
                        <div className="header-tabs__predictions">Прогнозы</div>
                    </div>
                    <div className="header-right">
                        <div className="header-right__search"><img src={searchImg} width="20" height="20" alt="search"/></div>
                        <div className="header-right__login">
                            <button className="header-button">Войти</button>
                        </div>  
                    </div>
                </div>
            </div>
        </header>
    </>
  );
}

export default Header;

import React from 'react';
import TabRouting from './TabRouting'

const  Matches = ({ matches, leagues }) => {
  return (
    <div className="matches">
      <TabRouting matches={matches} leagues={leagues} />
    </div>
  );
}

export default Matches;

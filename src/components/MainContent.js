import React from 'react';
import MyLeagueTabs from './MyLeagueTabs'
import CountryTabs from './CountryTabs';
import Matches from './Matches'
import axios from 'axios'

class MainContent extends React.Component {
  state ={
    matches: [],
    leagues: []
}

  componentDidMount() {
    axios.all([
      axios.get('http://u0362146.plsk.regruhosting.ru/match'),
      axios.get('http://u0362146.plsk.regruhosting.ru/league')
    ])
    .then(axios.spread((matchRes, leagueRes) => {
      // show both responses
      console.log(matchRes.data, 'fetch')
      const matchData = matchRes.data;
      const leagueData = leagueRes.data;

      this.setState({ 
        matches: matchData, 
        leagues: leagueData 
      })
    }))
    .catch(error => {
      console.log(error);
    })
  }
  toggleOpenTab = () =>{
    this.setState({
      isOpen: !this.state.isOpen
    })
  }
  render() {
    return (
      <div className="main-content">
        <div className="wrap">
          <div className="side-bar">
            <MyLeagueTabs matches={this.state.matches} leagues={this.state.leagues} toggleOpenTab={this.toggleOpenTab} isOpen={this.state.isOpen} />
            <CountryTabs matches={this.state.matches} leagues={this.state.leagues} toggleOpenTab={this.toggleOpenTab} isOpen={this.state.isOpen} />
          </div>
          <Matches matches={this.state.matches} leagues={this.state.leagues} />
        </div>
      </div>
    );
  }
}

export default MainContent;

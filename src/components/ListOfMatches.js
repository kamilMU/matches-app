import React from 'react';
import pic from '../flag/flag.jpg'
import star1 from '../flag/Star.jpg'
import star2 from '../flag/Star2.jpg'

export const ListOfMatches = (props) => {
  return (
    <>
      {props.content.map(league => 
        <div className="league-block" key={league.id}>

          {league.items.map(item =>
            <div key={item.id}>
            <div className="flag-and-tittle">
            <img className="flag-and-tittle__league-flag" src={pic}  alt="flag" />
            <div className="flag-and-tittle__league-tittle">{league.league + ': '}</div>
            <div>{item.item}</div> 
            </div>

            <div>{item.matches.map(match => 
              <div className="matches-list" key={match.id}>
                <div className="matches-list__time">{new Date(match.time).getHours() + ':' + new Date(match.time).getMinutes()}</div>
                <div className="matches-list__name">{match.name}</div>
                <div className="matches-list__score">{match.score === null ? null : match.score}</div>
                <div className="matches-list__favorite">{match.favorite === 0 ? <img src={star1} width="19" height="19" alt="star1"/> : <img src={star2} width="19" height="19" alt="star1" />}</div>
                <div className="matches-list__status">{match.status}</div>
                <div className="matches-list__link"><a href={match.link}>Подробнее</a></div>
              </div>)}
            </div>
            </div>)}
        </div>)}
    </>
  );
}


import React from 'react';
import { Route, Switch } from 'react-router-dom'
import { ListOfMatches } from './ListOfMatches'
import { isToday, isTomorrow } from 'date-fns'
import { NavLink } from 'react-router-dom'
import calendar from '../flag/Calendar.jpg'

class TabRouting extends React.Component {

  getMatches = (filterCriteria) => {
    const transformedLeagues = this.props.leagues.map(league => {
      return {
        ...league, 
        items: league.items.map(item => {
          return {
            ...item, 
            matches: this.props.matches.filter(match => { 
              if(filterCriteria) {
                return match.league_id === item.id && filterCriteria(new Date(match.time))
              } 
              return match.league_id === item.id
         })
        }
      })
    }
    })
    // console.log(transformedLeagues, 'transform')
    return transformedLeagues;
  }

  render() {
    let today = new Date();
    let days = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
    let dayOfTheWeek = days[today.getDay()];
    let date =today.getDate() + "." + parseInt(today.getMonth()+1) + " " + dayOfTheWeek;
    const numberOfTodayMatches = this.props.matches.filter(match => isToday(new Date(match.time))).length
    const numberOfTomorrowMatches = this.props.matches.filter(match => isTomorrow(new Date(match.time))).length
    return (
      <>
        <div className="tabs">
          <div className="main-content-tabs">
            <div className="tabs-left">
              <NavLink to='/today' className="tab" activeClassName="active">
                Сегодня <span className="quantity-of-data">{numberOfTodayMatches}</span>
              </NavLink>
              <NavLink to='/tomorrow' className="tab" activeClassName="active">
                <div>Завтра<span className="quantity-of-data">{numberOfTomorrowMatches}</span></div>
              </NavLink>
              <NavLink to='/allmatches' className="tab" activeClassName="active">
                <div>Все матчи <span className="quantity-of-data">{this.props.matches.length}</span></div>
              </NavLink>
            </div>
            <div className="calendar-and-date">
              <div><img src={calendar} alt="calen" width="19" height="19"/></div><div className="date">{ date }</div>
            </div>
          </div>
        </div>
        <div className="matches-content">
          <div className="content">
              <Switch>
                <Route path="/today" render={() => <ListOfMatches content={this.getMatches(isToday)}/>} />
                <Route path="/tomorrow" render={() => <ListOfMatches content={this.getMatches(isTomorrow)}/>} />
                <Route path="/allmatches"  render={() => <ListOfMatches content={this.getMatches()}/>} />
              </Switch>
          </div>
        </div>
      </>
    );
  }
}

export default TabRouting;
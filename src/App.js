import React from 'react';
import './App.css';
import Header from './components/Header'
import MainContent from './components/MainContent'

class App extends React.Component {
  state={
    isOpen: false
  }
  openMenu = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }
  render() {
    return (
      <div className="App">
        <Header toggle={this.openMenu} isOpen={this.state.isOpen} />
        <MainContent isMenuClicked={this.state.isMenuClicked} />
      </div>
    );
  }
}

export default App;
